let todoItemsContainer = document.getElementById("all");
let addBtnEl = document.getElementById("addBtn");
let active = document.getElementById("active");
let completed = document.getElementById("completed");

let todosList = [];

let todoCount = todosList.length;

function onTodoStatusChange(labelId) {
  let labelElement = document.getElementById(labelId);
  labelElement.classList.toggle("checked");
}

function createAndAppendTodo(todo) {
  let todoId = `todo${todo.uniqueNo}`;
  let checkBoxId = `checkBox${todo.uniqueNo}`;
  let labelId = `label${todo.uniqueNo}`;

  let todoItem = document.createElement("li");
  todoItem.classList.add("todo-item");
  todoItemsContainer.appendChild(todoItem);

  let checkboxEl = document.createElement("input");
  checkboxEl.setAttribute("id", checkBoxId);
  checkboxEl.setAttribute("type", "checkbox");
  checkboxEl.classList.add("check");
  todoItem.appendChild(checkboxEl);

  let labelEl = document.createElement("label");
  labelEl.setAttribute("id", labelId);
  labelEl.classList.add("label");
  labelEl.textContent = todo.text;
  labelEl.setAttribute("for", checkBoxId);
  todoItem.appendChild(labelEl);

  checkboxEl.onclick = function () {
    onTodoStatusChange(labelId);
  };

  let deleteContainer = document.createElement("div");
  deleteContainer.classList.add("delete-icon-container");
  todoItem.appendChild(deleteContainer);

  let icon = document.createElement("i");
  icon.classList.add("far", "fa-trash-alt", "delete-icon");
  deleteContainer.appendChild(icon);

}

for (let todo of todosList) {
  createAndAppendTodo(todo);
}

function openTab(evt, tabName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tab");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByTagName("button");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " active";
}

function onAddTodo() {
  let inputEl = document.getElementById("inputEl");
  let userInput = inputEl.value;

  if (userInput === "") {
    alert("Enter a valid");
  }
  todoCount += 1;

  let newTodo = {
    text: userInput,
    uniqueNo: todoCount,
    isChecked: false,
  };

  createAndAppendTodo(newTodo);
}

addBtnEl.addEventListener("click", function () {
  onAddTodo();
});
